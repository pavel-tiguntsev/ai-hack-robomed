#/usr/bin/python3
# -*- coding: utf-8 -*- 

import csv
import re
from bson.objectid import ObjectId
from myconfig import remoteURL, localURL

from pymongo import MongoClient
client = MongoClient(localURL)
db = client.reckonfood
fields_we_need = ["Белки", "Жиры", "Углеводы",	"Калорийность", "Вес", "Стоимость"]


def getDishMenu():
    return db.dishes.distinct("type")


def reckon_dish(dish_id):
    dish = {"Белки": 0, "Жиры":0, "Углеводы":0,	"Калорийность":0, "Стоимость": 0, "Вес": 0}
    item = db.dishes.find_one({'_id': ObjectId(dish_id)})
    
    if item['type'] == u"Напитки":
        return dish
    else:
        ings = item['ings']

        for ing in ings:
            dish['Вес'] += float(ing['q'])
            dish['Белки'] += count_ingQ(ing, 'Белки', ing['q'])
            dish['Жиры'] += count_ingQ(ing, 'Жиры', ing['q'])
            dish['Углеводы'] += count_ingQ(ing, 'Углеводы', ing['q'])
            dish['Калорийность'] += count_ingQ(ing, 'Калорийность', ing['q'])
            dish['Стоимость'] += count_ingQ(ing, 'Цена  за 100 гр', ing['q'])


        return dish


def summarize_dishes(dishes_ids):
    summarize = {}
    dishes = []
    for d in dishes_ids:
        dishes.append(reckon_dish(d))

    for f in fields_we_need:
        summarize[f] = sums_field(dishes, f)

    decoded = {}

    for k in summarize.keys():
        decoded[k.decode('utf8')] = round(summarize[k], 2)

    decodeddishes = []

    for d in dishes:
        dis = {}
        for k in d:
            dis[k.decode('utf8')] = d[k]
        decodeddishes.append(dis)

    return [decoded, decodeddishes]

def count_ingQ(ing, field, q):
    product = get_product(ing['product'])
    if product == None:
        return 0
    return float(get_product((ing['product']))[field.decode('utf8')]) * (float(q)/100)


def sums_field(dishes, filed_name):
    sum = 0
    for p in dishes:
        sum += float(p[filed_name])
    return sum

def get_product(product_name):
    # print(product_name)
    regx = re.compile(product_name, re.IGNORECASE)
    return db.products.find_one({'Наименование': regx})
