# -*- coding: utf-8 -*-
import csv
from pymongo import MongoClient

from myconfig import remoteURL, localURL

client = MongoClient(localURL)
db = client.reckonfood
dish_list = []
db.dishes.remove({"type": "drink"})
header = ["name", "Белки", "Жиры", "Углеводы", "Калорийность", "Объем, л", "Цена", "Цена за 100 гр"]


with open('./csv/recs/drinks.csv', newline='', encoding='utf-8') as f:
    reader = csv.reader(f)
    drinks = []

    for ind, row in enumerate(reader):
        drink = {}
        drink["type"] = 'Напитки'
        for hn, h in enumerate(header):
            drink[h] = row[hn]
        
        drinks.append(drink)


db.dishes.insert_many(drinks)
