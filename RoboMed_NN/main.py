import tensorflow as tf;
import pandas as pd
import numpy as np
import sys
import os
import io
import gensim, logging
import keras
from gensim.models import Word2Vec
from keras.models import Sequential
from keras.layers import Dense, Activation,Conv2D,MaxPooling2D,Flatten,Dropout
from sklearn.feature_extraction.text import CountVectorizer
from functools import reduce
from pathlib import Path
COUNTLENGTHCHARS = 18

labels = ['Возраст', 'Пол', 'Общее состояние', 'аллергия', 'Анамнез заболевания', 'Внешний осмотр', 'Код_диагноза']

def getCharDiagnos(code):
    charCode = code[0]
    intCode = int(code[1:2])
#    vec = [0 for x in range(COUNTLENGTHCHARS)]
    if charCode == 'A' or charCode == 'B':
        return 0
    elif charCode == 'C' or (charCode == 'D' and intCode <= 48):
        return 1
    elif charCode == 'D' and intCode >= 51 and intCode <=89 :
        return 2
    elif charCode == 'E' and intCode >= 00 and intCode <= 90:
        return 3
    elif charCode == 'F':
        return 4
    elif charCode == 'G':
        return 5
    elif charCode == 'H' and intCode >= 00 and intCode <= 59:
        return 6
    elif charCode == 'H' and intCode >= 60 and intCode <= 95:
        return 7
    elif charCode == 'I':
        return 8
    elif charCode == 'J':
        return 9
    elif charCode == 'K' and intCode >= 00 and intCode <= 93:
        return 10
    elif charCode == 'L':
        return 11
    elif charCode == 'M':
        return 12
    elif charCode == 'N':
        return 13
    elif charCode == 'P' and intCode >= 00 and intCode <= 96:
        return 14
    elif charCode == 'Q':
        return 15
    elif charCode == 'R':
        return 16
    elif charCode == 'T' and intCode >= 00 and intCode <= 96:
        return 17
    return 19

def f(x):
    if type(x) is float:
        return 'Nan'
    else:
        return x.split()


def main(argv):
    dataset = pd.read_csv('test_data.csv')
    l = list(map(f, list(dataset[labels[2]].values)))   #.append(map(f, list(dataset[labels[4]].values)))
    anamnesis = list(map(f, list(dataset[labels[5]].values)))
    diagnos = list(dataset[labels[6]].values)
    mFile = Path('model.bin')
    if not mFile.exists() or len(argv) > 1:
        print ("generate new model")
        model = Word2Vec(l + anamnesis, min_count=10, size=200)
        model.save('model.bin')
        with open("dictionary.txt", "w") as the_file:
            for word in model.wv.vocab,keys():
                the_file.write(word)
    else:
        print ("load model")
        model = gensim.models.Word2Vec.load('model.bin')
    vec = [0 for x in range(200)]
    x_train = []
    y_train = []
    for i in range(len(l)):
        if getCharDiagnos(diagnos[i]) == 19:
            continue;
        line = anamnesis[i]
        diagnos_code = diagnos[i]
        for word in line:
            try:
                vec += model[word]
            except KeyError:
                continue
        for j in range(len(vec)):
            if abs(vec[j]) > 1:
                vec[j] = 1/vec[j]
        x_train.append(vec);
        y_train.append(getCharDiagnos(diagnos_code))



        #x = vectorizer.fitfit_transform(data)
    x_train = np.array(x_train).reshape(len(y_train), 200)
    y_train = keras.utils.to_categorical(y_train, num_classes=COUNTLENGTHCHARS);
    print ('generating model')

    modelKeras = Sequential()

    modelKeras.add(Dense(128, activation='relu', input_shape=(200,)))
    modelKeras.add(Dense(256, activation='relu'))
    modelKeras.add(Dropout(0.5))
    modelKeras.add(Dense(64, activation='sigmoid'))
    modelKeras.add(Dense(COUNTLENGTHCHARS, activation='softmax'))

    modelKeras.compile(loss='categorical_crossentropy', optimizer='rmsprop')

#    with open("file for json.js", "w") as the_file:
#        the_file.write(modelKeras.to_json())
    print (len(y_train))
    print (y_train[0])
    print (len(x_train[0]))
    print (type(x_train))
    print ('fit')
    modelKeras.fit(x_train, y_train, epochs=10, verbose=2, batch_size=128)
    print ('fit is complite')

    while 1==1:
        word = input('input your word: ')
        if word == 'exit':
            break;
        try:
            print (modelKeras.predict(model[word]))
        except KeyError:
            print ('KeyError')
    print ("model is trained")


main(sys.argv)
